# CONTACTS CRYPTER DECRYPTER

GUI program written in Python

# Features

* Crypt a given Phone number according to a given key and save it into
 a .vcf file
* Decrypt a previously encrypted Phone number and save it into a .vcf file,
same key should be used

# Additional infos

* For GNU/Linux users Tkinter library should be installed :

                Arch/Arch based distros : sudo pacman -S tk
                Debian/Debian based distros : sudo apt install python3-tk

* Key should be between 0 and 99, if more than 2 digits are given only the first
2 will be taken in consideration
* "+" isn't allowed, write "00" instead
* Unicodes are used, use compatible font  
