# coding:utf-8

import tkinter
from tkinter import messagebox
from random import randrange


# Main Window
App = tkinter.Tk()
App.geometry("800x600")
App.title("Phone Number Crypter Decrypter")

# Process for ecncryption


def Afficheur(*args):
    var2.set(int(cle.get()))


Dico = dict()
Dico_ = dict()


def makedico():
    """make 2 dictionnaries for encryption"""
    if len(var2.get()) > 2:
        var2.set(var2.get()[:2])

    key = int(var2.get())

    for i in range(0, 10):
        Dico[i] = chr(int((key+65)*3 + (i+1)**2.5))

    # 192-847

    Dicokeys = [keys for keys in Dico.keys()]
    Dicovalues = [values for values in Dico.values()]

    Dico_inter = dict(zip(Dicovalues, Dicokeys))
    Dico_.clear()
    Dico_.update(Dico_inter)


cle = tkinter.StringVar()
var_ = tkinter.StringVar()
var2 = tkinter.StringVar()
cle.trace("w", Afficheur)
Entry = tkinter.Entry(App, textvariable=cle)
Bouton = tkinter.Button(App, text="Submit", command=makedico)

# Encrypt Window


def Encrypt_Window():
    """Encryption Window"""
    EncryptWindow = tkinter.Toplevel(App)
    EncryptWindow.geometry("400x300")
    EncryptWindow.title("Encrypt")
    Phone_number = tkinter.StringVar()
    Phone_number_encrypted = tkinter.StringVar()
    Name = tkinter.StringVar()

    Entry0 = tkinter.Entry(EncryptWindow, textvariable=Name)
    Entry1 = tkinter.Entry(EncryptWindow, textvariable=Phone_number)
    Entry2 = tkinter.Entry(EncryptWindow, textvariable=Phone_number_encrypted, show="*")

    Label0 = tkinter.Label(EncryptWindow, text="Name")
    Label1 = tkinter.Label(EncryptWindow, text="Phone Number")
    Label2 = tkinter.Label(EncryptWindow, text="Crypted Phone Number")

    def Tracer(*args):
        """Write a crypted version of your input into the third entry"""

        Phone_number_encrypted.set("")

        for j in Phone_number.get():
            Phone_number_encrypted.set(Phone_number_encrypted.get()+str(Dico[int(j)]))

    Phone_number.trace("w", Tracer)

    def Encrypt():
        """Write the coded into E_contacts.vcf file located in this directory"""

        Info = messagebox.showinfo(
            App, message="Your Crypted Phone Number has been saved under ./Encrypted_Contacts.vcf")

        try:
            Info.pack()
        except AttributeError:
            pass
        f = open("Encrypted_Contacts.vcf", "a")
        f.write("BEGIN:VCARD\nVERSION:2.1\nN:;{};;;\nFN:{}\nTEL;CELL;PREF:{}\n".format(
            Name.get(), Name.get(), Phone_number_encrypted.get()))
        f.close()

    Encrypt_Button = tkinter.Button(EncryptWindow, text="Encrypt and save", command=Encrypt)

    Label0.pack()
    Entry0.pack()
    Label1.pack()
    Entry1.pack()
    Label2.pack()
    Entry2.pack()
    Encrypt_Button.pack()

# Decrypt Window


def Decrypt_Window():
    """Decrypting Window"""
    DecryptWindow = tkinter.Toplevel(App)
    DecryptWindow.geometry("400x300")
    DecryptWindow.title("Decrypt")

    Name_ = tkinter.StringVar()
    CryptedPhone = tkinter.StringVar()
    DecryptedPhone = tkinter.StringVar()

    Entry0_ = tkinter.Entry(DecryptWindow, textvariable=Name_)
    EntryD1 = tkinter.Entry(DecryptWindow, textvariable=CryptedPhone)
    EntryD2 = tkinter.Entry(DecryptWindow, textvariable=DecryptedPhone, show="*")

    Label0_ = tkinter.Label(DecryptWindow, text="Name")
    Label1_ = tkinter.Label(DecryptWindow, text="Crypted Phone Number")
    Label2_ = tkinter.Label(DecryptWindow, text="Decrypted Phone Number")

    def Traceur(*args):
        DecryptedPhone.set("")

        for z in CryptedPhone.get():
            try:
                DecryptedPhone.set(DecryptedPhone.get()+str(Dico_[z]))
            except KeyError:

                DecryptedPhone.set(DecryptedPhone.get()+str(randrange(0, 10)))

    CryptedPhone.trace("w", Traceur)

    def Decrypt():
        """Write the coded into E_contacts.vcf file located in this directory"""

        Info_ = messagebox.showinfo(
            App, message="Your Decrypted Phone Number has been saved under ./Decrypted_Contacts.vcf")

        try:
            Info_.pack()
        except AttributeError:
            pass

        f = open("Decrypted_Contacts.vcf", "a")

        c = 0

        for elements in CryptedPhone.get():
            if elements not in Dico_:
                c += 1
                break

        if c > 0:
            f.write("BEGIN:VCARD\nVERSION:2.1\nN:;{};;;\nFN:{}\nTEL;CELL;PREF:0{}\n".format(
                Name_.get(), Name_.get(), DecryptedPhone.get()[:-1]))
        else:
            f.write("BEGIN:VCARD\nVERSION:2.1\nN:;{};;;\nFN:{}\nTEL;CELL;PREF:{}\n".format(
                Name_.get(), Name_.get(), DecryptedPhone.get()))

        f.close()

    Decrypt_Button = tkinter.Button(DecryptWindow, text="Decrypt and save", command=Decrypt)

    Label0_.pack()
    Entry0_.pack()
    Label1_.pack()
    EntryD1.pack()
    Label2_.pack()
    EntryD2.pack()
    Decrypt_Button.pack()

# Main Window's widgets


Encrypt_Mode = tkinter.Button(App, text="Crypt a phone number", command=Encrypt_Window)
Decrypt_Mode = tkinter.Button(App, text="Decrypt a phone number", command=Decrypt_Window)

LabelKey = tkinter.Label(App, text="Enter your key below before choosing mode")

LabelKey.pack(pady=2)
Entry.pack()
Bouton.pack()
Encrypt_Mode.pack(pady=10)
Decrypt_Mode.pack(pady=10)
App.mainloop()
